# Skeleton Program for the AQA AS Summer 2018 examination
# this code should be used in conjunction with the Preliminary Material
# written by the AQA AS Programmer Team
# developed in a Python 3 environment

# Version Number : 1.6
import sys
import tkinter as tk
from tkinter.filedialog import askopenfilename
from tkinter import messagebox

root = tk.Tk()
root.withdraw()
root.lower()

SPACE = ' '
EOL = '#' # end of line
EMPTYSTRING = ''

def ReportError(s): # s is the error that is reported
  print('{0:<5}'.format('*'),s,'{0:>5}'.format('*'))
  # print 5 spaces to the left of s, after a *
  # print 5 spaces to the right of s followed by a *

def StripLeadingSpaces(Transmission): # takes the line from the file, removes the spaces at the beginning.
  TransmissionLength = len(Transmission) # works out the length
  if TransmissionLength > 0: # checks the message exists
    FirstSignal = Transmission[0] # first character = first position
    while FirstSignal == SPACE and TransmissionLength > 0:
      TransmissionLength -= 1
      Transmission = Transmission[1:]# string splicing (2nd char - end)
      if TransmissionLength > 0:
        FirstSignal = Transmission[0] # change FirstSignal to new first letter
  if TransmissionLength == 0: # no message
    ReportError("No signal received") # line 12
  return Transmission # return the stripped string (no leading spaces), sent to line 43

def StripTrailingSpaces(Transmission): # remove spaces at the end
  LastChar = len(Transmission) - 1 # find the last char
  while Transmission[LastChar] == SPACE:
    LastChar -= 1
    Transmission = Transmission[:-1] # from the beginning to one off the end
  return Transmission # sent back to line 45

def GetTransmission(): # open the file and read the morse code
  FileName = askopenfilename(filetypes = [("Text files", "*.txt"), ("All Files", "*.*")])
  try:
    FileHandle = open(FileName, 'r')
    Transmission = FileHandle.readline() # read a line from the file
    FileHandle.close()
    Transmission = Transmission.replace("#", "")
    Transmission = StripLeadingSpaces(Transmission) # line 15
    if len(Transmission) > 0: # check a message was returned
      Transmission = StripTrailingSpaces(Transmission) # line 30
      Transmission = Transmission + EOL # concatenate '#', useful later?
  except:
    ReportError("No transmission found") # line 12
    Transmission = EMPTYSTRING # force the Transmission to be an empty string - useful later?
  return Transmission # return to line 109.
def GetNextSymbol(i, Transmission):
  if Transmission[i] == EOL:
    print()
    print("End of transmission")
    Symbol = EMPTYSTRING
  else:
    SymbolLength = 0
    Signal = Transmission[i]
    while Signal != SPACE and Signal != EOL:
      i += 1
      Signal = Transmission[i]
      SymbolLength += 1
    if SymbolLength == 1:
      Symbol = '.'
    elif SymbolLength == 3:
      Symbol = '-'
    elif SymbolLength == 0:
      Symbol = SPACE
    else:
      ReportError("Non-standard symbol received")
      Symbol = EMPTYSTRING
  return i, Symbol

def GetNextLetter(i, Transmission):
  SymbolString = EMPTYSTRING # contain the final string of dots/dashes from a block taken from the encoded Transmission string
  LetterEnd = False
  while not LetterEnd:
    i, Symbol = GetNextSymbol(i, Transmission)
## Symbol will be either a single dash or dot
    if Symbol == SPACE:
      LetterEnd = True
      i += 4
    elif Transmission[i] == EOL:
      LetterEnd = True
    elif Transmission[i + 1] == SPACE and Transmission[i + 2] == SPACE:
      LetterEnd = True
      i += 3
    else:
      i += 1
    SymbolString = SymbolString + Symbol
  return i, SymbolString

def Decode(CodedLetter, Dash, Letter, Dot):
  CodedLetterLength = len(CodedLetter)
  Pointer = 0
  for i in range(CodedLetterLength):
    Symbol = CodedLetter[i] # dot/dash at index i in CodedLetter
    if Symbol == SPACE:
      return SPACE # Why would the function receive a SPACE in the first place?
## The following code will traverse through the tree
## using dash (left) and dot (right) to navigate.
## Pointer will be set to the new letter found at
## that node.
    elif Symbol == '-':
      Pointer = Dash[Pointer]
    else:
      Pointer = Dot[Pointer]
  return Letter[Pointer] # return the letter corresponding with the Pointer value; that is, the letter found at the alphabetical position.

def ReceiveMorseCode(Dash, Letter, Dot): # Dash, Letter and Dot are arrays.
  PlainText = EMPTYSTRING # Final decoded message
  MorseCodeString = EMPTYSTRING # File input converted to morse code
  Transmission = GetTransmission() # line 35
  LastChar = len(Transmission) - 1 # works out the length of the message purely for use in the while loop.

  i = 0 # used as an index for the character in Transmission
  while i < LastChar: # i is < len(Transmission)
    i, CodedLetter = GetNextLetter(i, Transmission) # line 75
    # CodedLetter is the dot/dash version of "=" and " " from the string Transmission.
    MorseCodeString = MorseCodeString + SPACE + CodedLetter
    PlainTextLetter = Decode(CodedLetter, Dash, Letter, Dot)
    PlainText = PlainText + PlainTextLetter
  print(MorseCodeString)
  print(PlainText)

def SendMorseCode(MorseCode):
  PlainText = input("Enter your message (uppercase letters and spaces only): ")
  PlainTextLength = len(PlainText)
  MorseCodeString = EMPTYSTRING
  for i in range(PlainTextLength):
    PlainTextLetter = PlainText[i]
    if PlainTextLetter == SPACE:
      Index = 0
    else:
      Index = ord(PlainTextLetter) - ord('A') + 1
    CodedLetter = MorseCode[Index]
    MorseCodeString = MorseCodeString + CodedLetter + SPACE
  print(MorseCodeString)

def DisplayMenu():
  print()
  print("Main Menu")
  print("=========")
  print("R - Receive Morse code")
  print("S - Send Morse code")
  print("X - Exit program")
  print("Z-turn morse code into words")
  print()

def DisplayGraphicalMenu(Dash,Dot,Letter,MorseCode):
    root.title("Morse Code translator")
    tk.Button(root , text="Recieve Morse Code", command=lambda a=Dash,b=Dot,c=Letter:RecieveMorseCode(a,b,c),width=20).pack()
    tk.Button(root , text="Send Morse Code", command=lambda a=Dash,b=Dot,c=Letter:RecieveMorseCode(a,b,c),width=20).pack()

def GetMenuOption():
  MenuOption = EMPTYSTRING
  while len(MenuOption) != 1:
    MenuOption = input("Enter your choice: ")
  return MenuOption

def SendReceiveMessages():
  Dash = [20,23,0,0,24,1,0,17,0,21,0,25,0,15,11,0,0,0,0,22,13,0,0,10,0,0,0]
  Dot = [5,18,0,0,2,9,0,26,0,19,0,3,0,7,4,0,0,0,12,8,14,6,0,16,0,0,0]
  Letter = [' ','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z']

  MorseCode = [' ','.-','-...','-.-.','-..','.','..-.','--.','....','..','.---','-.-','.-..','--','-.','---','.--.','--.-','.-.','...','-','..-','...-','.--','-..-','-.--','--..']

  DisplayGraphicalMenu(Dash,Dot,Letter,MorseCode)
##  ProgramEnd = False
##  while not ProgramEnd: # Menu, continually run allowing the user to keep sending or receiving
##    DisplayMenu() # Displays the menu
##    MenuOption = GetMenuOption() # Get user input
##    # Call function appropriate to menu letter choice
##    if MenuOption == 'R':
##      ReceiveMorseCode(Dash, Letter, Dot) # line 104
##    elif MenuOption == 'S':
##      SendMorseCode(MorseCode)
##    elif MenuOption == 'X':
##      ProgramEnd = True
##    elif MenuOption=='Z':
##      Convert(Letter,MorseCode)

def Convert(Letter, MorseCode):
    x=''
    morse=input("Enter morse code to convert: ")
    n=morse.split("   ")
    for j in range(len(n)):
        p=n[j].split(" ")
        for q in range(len(p)):
            for i in range(len(MorseCode)):
               if p[q]==MorseCode[i]:
                  x=x+Letter[i]
        x=x+' '
    print(morse)
    print("in alphabets is=")
    print(x)

if __name__ == "__main__":
  SendReceiveMessages()