SET setup_file="C:\Users\%USERNAME%\Desktop\Ansible_setup.ps1" 
@echo off
@echo $url = "https://raw.githubusercontent.com/ansible/ansible/devel/examples/scripts/ConfigureRemotingForAnsible.ps1" > %setup_file%
@echo $file = "$env:temp\ConfigureRemotingForAnsible.ps1" >> %setup_file%
@echo (New-Object -TypeName System.Net.WebClient).DownloadFile($url, $file) >> %setup_file%
@echo powershell.exe -ExecutionPolicy ByPass -File $file >> %setup_file%
@echo winrm enumerate winrm/config/Listener >> %setup_file%
@echo echo "Ansible client setup has been completed" >> %setup_file%
@echo pause >> %setup_file%
powershell -command "& {Set-ExecutionPolicy -ExecutionPolicy Unrestricted -Force}"
powershell -Command "Start-Process powershell.exe -ArgumentList "%setup_file%"" -Verb RunAs